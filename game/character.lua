Character = class(Entity, function(my, imgfile)
	my.baseImg = love.graphics.newImage(imgfile)
	

	my.imgs = {}
	for i=1,4 do
		my.imgs[i] = {}
		for j=1,4 do
			my.imgs[i][j] = imageSection(my.baseImg, (i-1)*0.25, (j-1)*0.25, 0.25, 0.25)
		end
	end

	my.move = {
		pos = {500, 500},
		dir = "down",
		speed = 150
	}
	
	my.anim = {
		active = true,
		frame = 1,
		timer = 0,
		delay = 0.1
	}

end)

function Character:copyOver(instance)
	instance._base.imgs = instance.imgs
	instance._base.anim = instance.anim
	instance._base.move = instance.move
end


function Character:getHalfWidth()
	return 24
end

function Character:getHalfHeight()
	return 24
end

function Character:getBottom()
	return 40
end

function Character:update(dt, anim)
	if not anim then return end
	-- walk animation
	if anim.active then
		anim.timer = anim.timer + dt
		if anim.timer > anim.delay then
			anim.frame = anim.frame + 1
			if anim.frame > 4 then anim.frame = 1 end
			anim.timer = anim.timer - anim.delay
		end
	end
end

function Character:draw()
	local pos = self.move.pos
	love.graphics.draw(self.imgs[dirInt(self.move.dir)][self.anim.frame], 
			pos[1], pos[2], 0, 1, 1, 24, 24) 
end

