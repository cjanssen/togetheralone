Entity = class(function (my)
end)

function Entity:load()
end
function Entity:update(dt)
end
function Entity:draw()
end

List = class(function (my)
my.list = {}
end)

function List:load()
	for i,v in ipairs(self.list) do
		v:load()
	end
end
function List:update(dt)
	for i,v in ipairs(self.list) do
		v:update(dt)
	end
end

function List:draw()
	for i,v in ipairs(self.list) do
		v:draw()
	end
end

function List:add(ent)
	if ent.list then
		for i,v in ipairs(ent.list) do
			table.insert(self.list, v)
		end
	else
		table.insert(self.list, ent)
	end
end

function List:remove(ent)
	for i,v in ipairs(self.list) do
		if v == ent then 
			table.remove(self.list, ent) 
			break
		end
	end
end


