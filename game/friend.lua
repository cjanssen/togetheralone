Friend = class(Character, function(my, imgfile)
	my._base.init(my, imgfile)
	my.move.pos = {math.random(1000),math.random(500)}

	my.state = "follow"
	my.leaveTimer = 25

	Character:copyOver(my)
end)


Friends = List()
function Friends:load()
	betty = Friend("img/amanda_0.png")
	dante = Friend("img/luke_0.png")

	betty.other = dante
	dante.other = betty
	dante.leaveTimer = 40
	Friends:add(betty)
	Friends:add(dante)

	abouttoleave = true
	AllEntities:add(Friends)
end

function Friend:update(dt)
	local distToPlayer = dist(player.move.pos, self.move.pos)
	local dirVec = {0,0}

	if love.keyboard.isDown("l") then abouttoleave = true end

	if abouttoleave then
		self.leaveTimer = self.leaveTimer - dt
		if self.leaveTimer < 0 then self.state = "leave" end
	end
	
	if self.state == "together" then
		self.anim.active = false
		if distToPlayer > threshFar then
			self.state = "follow"
		end
	elseif self.state == "follow" then
		-- choose direction
		self.anim.active = true

		local friendVec = normalize({self.other.move.pos[1] - self.move.pos[1],
									 self.other.move.pos[2] - self.move.pos[2]})

		local mp = {player.move.pos[1] - friendVec[1] * threshNear * 0.5, 
					player.move.pos[2] - friendVec[2] * threshNear * 0.5}
		dirVec = normalize({mp[1] - self.move.pos[1], mp[2] - self.move.pos[2] })

		
		if distToPlayer < threshNear then
			self.state = "together"
		end
	elseif self.state == "leave" then
		self.anim.active = true
		dirVec = normalize({self.move.pos[1] - player.move.pos[1], self.move.pos[2] - player.move.pos[2] })
	end

	if self.anim.active then
		-- look
		if math.abs(dirVec[1]) > math.abs(dirVec[2]) then
			--horizontal
			if dirVec[1] < 0 then
				self.move.dir = "left"
			else
				self.move.dir = "right"
			end
		else
			--vertical
			if dirVec[2] < 0 then
				self.move.dir = "up"
			else
				self.move.dir = "down"
			end
		end

		-- move
		self.move.pos = { self.move.pos[1] + dirVec[1] * self.move.speed * dt, self.move.pos[2] + dirVec[2] * self.move.speed * dt }
	end

	self._base:update(dt, self.anim)
end
