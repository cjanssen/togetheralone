function include( filename )
	love.filesystem.load( filename )()
end

function love.load()
	include("class.lua")
	include("entitylist.lua")

	include("utils.lua")
	include("tree.lua")
	include("character.lua")
	include("friend.lua")
	include("player.lua")	
	AllEntities = List()
	player:load()
	Friends:load()
	Trees:load()	
	--AllEntities:load()
end


function compareEntities(a,b)
  return a.move.pos[2] + a:getBottom() < b.move.pos[2] + b:getBottom()
end

function love.update(dt)
	table.sort(AllEntities.list, compareEntities)
	AllEntities:update(dt)

	if love.keyboard.isDown("escape") then
		love.event.push("quit")
	end
end

function love.draw()
	love.graphics.setColor(17, 91, 44)
	love.graphics.rectangle("fill", 0,0,1024,768)

	love.graphics.setColor(255,255,255)
--	love.graphics.rectangle("fill", 100, 100, 100, 100)
	AllEntities:draw()
end

