Player = class(Character, function(my)
	my._base.init(my,"img/sabrina_0.png")
	Character:copyOver(my)
end)


function dirInt(dir)
	if dir == "down" then return 1 end
	if dir == "left" then return 2 end
	if dir == "up" then return 3 end
	if dir == "right" then return 4 end
end

player = Player()
function Player:load()
	AllEntities:add(player)
end

--Trees:add(player)

function Player:update(dt)
	-- select direction
	self.anim.active = false
	if love.keyboard.isDown("down") then
		self.anim.active = true
		self.move.dir = "down"
		self.move.pos[2] = self.move.pos[2] + self.move.speed * dt
	end

	if love.keyboard.isDown("up") then
		self.anim.active = true
		self.move.dir = "up"
		self.move.pos[2] = self.move.pos[2] - self.move.speed * dt
	end

	if love.keyboard.isDown("left") then
		self.anim.active = true
		self.move.dir = "left"
		self.move.pos[1] = self.move.pos[1] - self.move.speed * dt
	end

	if love.keyboard.isDown("right") then
		self.anim.active = true
		self.move.dir = "right"
		self.move.pos[1] = self.move.pos[1] + self.move.speed * dt
	end



	self._base:update(dt, self.anim)
end


