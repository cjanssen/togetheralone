Trees = List()

Tree = class(Entity, function(my)
	local n = math.random(9)
	my.img = love.graphics.newImage("img/tree_"..n..".png")
	my.move = {
		pos = {math.random(1024), math.random(768)}
	}
end)

function Tree:getHalfWidth()
	return self.img:getWidth() * 0.5
end

function Tree:getHalfHeight()
	return self.img:getHeight() * 0.5
end

function Tree:getBottom()
	return self:getHalfHeight()
end

function Trees:load()
	for i=1,70 do
		Trees:add(Tree())
	end
	AllEntities:add(Trees)
end

function Tree:draw()
	love.graphics.draw(self.img, self.move.pos[1],self.move.pos[2], 0, 1, 1, self:getHalfWidth(), self:getHalfHeight())
end
