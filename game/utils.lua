function imageSection( image, xfraction, yfraction, wfraction, hfraction )
--	local image = love.graphics.newImage(filename)
	local x = math.floor(xfraction * image:getWidth())
	local y = math.floor(yfraction * image:getHeight())
	local w = math.floor(wfraction * image:getWidth())
	local h = math.floor(hfraction * image:getHeight())
	
	local img = love.graphics.newCanvas( w, h )
	love.graphics.setCanvas( img )
	img:clear()
	love.graphics.draw( image, -x, -y)
	
	
--	local img = love.graphics.newImage( frameBuffer:getImageData() )
	love.graphics.setCanvas()
	return img
end

threshFar = 150
threshNear = 75

function dist(posA, posB)
	local dx = posA[1] - posB[1]
	local dy = posA[2] - posB[2]
	return math.sqrt(dx*dx + dy*dy)
end

function normalize(vec)
	local mod = dist(vec,{0,0})
	if mod > 0 then
		return {vec[1]/mod, vec[2]/mod}
	else
		return vec
	end
end

